import axios from 'axios'

const axiosApi = axios.create({
  baseURL: process.env.ROOT_URL
})

axiosApi.interceptors.request.use(
  function (config) {
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

export const setAuthHeader = (token) => {
  if (!token) return
  axiosApi.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

// Set the initial header from storage or something (should surround with try catch in actual app)
setAuthHeader(localStorage.getItem('token'))

export default axiosApi

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueJwtDecode from 'vue-jwt-decode'
import Login from '@/components/Login'
import Boats from '@/components/Boats'
import store from '../store/index'

Vue.use(VueRouter)

const Router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/boats',
      name: 'Boats',
      component: Boats,
      meta: {
        isAuthenticated: true
      }
    }
  ]
})

Router.beforeEach((to, from, next) => {
  if (to.meta.isAuthenticated) {
    let userData = decodeJwtToken()
    if (userData) {
      if (userData['exp'] <= (Date.now() / 1000 + 60)) { // still 60 seconds the token is valid, we are refreshing it
        store.dispatch('auth/refreshToken', {token: localStorage.getItem('token')})
          .then(resRefreshToken => {
            if (resRefreshToken.data && resRefreshToken.data.token) {
              next()
            } else {
              store.dispatch('auth/signOut')
              next('/')
            }
          })
          .catch(() => {
            store.dispatch('auth/signOut')
            next('/')
          })
      } else {
        // need to check token validation here
        store.dispatch('auth/verifyToken', {token: localStorage.getItem('token')})
          .then(res => {
            if (res.data && res.data.token) {
              next()
            } else {
              store.dispatch('auth/signOut')
              next('/')
            }
          })
          .catch(() => {
            store.dispatch('auth/signOut')
            next('/')
          })
      }
    } else {
      store.dispatch('auth/signOut')
      next('/')
    }
  } else {
    next()
  }
})
function decodeJwtToken () {
  const token = localStorage.getItem('token')
  if (token) {
    return VueJwtDecode.decode(token)
  }
  return null
}

export default Router

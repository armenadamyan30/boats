import axiosApi from '../../axiosApi'

// initial state
const state = {
  token: null
}
// getters
const getters = {
  getToken: (state) => {
    return state.token
  }
}
// actions
const actions = {
  signIn ({commit, state}, payload) {
    return axiosApi.post(`/api/auth/api-token-auth/`, payload)
      .then(response => {
        if (response.data && response.data.token) {
          commit('setToken', response.data.token)
        }
        return response
      })
      .catch(error => {
        return error
      })
  },
  signOut ({commit, state}, payload) {
    localStorage.removeItem('token')
    commit('setToken', null)
  },
  verifyToken ({commit, state}, payload) {
    return axiosApi.post(`/api/auth/api-token-verify/`, {token: payload.token})
      .then(response => {
        if (response.data && response.data.token) {
          commit('setToken', response.data.token)
        }
        return response
      })
      .catch(error => {
        return error
      })
  },
  refreshToken ({commit, state}, payload) {
    return axiosApi.post(`/api/auth/api-token-refresh/`, {token: payload.token})
      .then(response => {
        if (response.data && response.data.token) {
          commit('setToken', response.data.token)
        }
        return response
      })
      .catch(error => {
        return error
      })
  }
}
// mutations
const mutations = {
  setToken (state, token) {
    localStorage.setItem('token', token)
    state.token = token
  }
}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

// initial state
import axiosApi from '../../axiosApi'

const state = {

}
// getters
const getters = {

}
// actions
const actions = {
  getBoats ({commit, state}, payload) {
    return axiosApi.get(`/boatgod/api/rest/boats/?format=json`, payload)
      .then(response => {
        return response
      })
      .catch(error => {
        return error
      })
  }
}
// mutations
const mutations = {

}
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
